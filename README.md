# URL Shorter

A simple URL Shorter I made to learn something more about ejs, express and MongoDB

## Installation

Open the root directory and run.

```bash
npm install
```

## Usage

Start up the server by running
```bash
npm devStart
```

A local webserver will start on port 5000

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

